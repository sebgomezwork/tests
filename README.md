<img src="./Java-Logo.png" alt="Logo" width="150">
<img src="spring-boot.png" alt="Logo" width="150">

# Especialización Backend
Proyecto plantilla para desarrollos de Baufest en tecnología Java.

Referencia:
- [Confluence](https://baufest.atlassian.net/wiki/spaces/SWDEV/pages/2987819030/Arquitecturas+de+referencia)
- [OneDrive](https://baufestcloud.sharepoint.com/sites/BaunasCloud/UPDevelopment/Shared%20Documents/Forms/AllItems.aspx?csf=1&web=1&e=sMNgOi&cid=85797ccd%2D5f53%2D4856%2Da7d1%2D80be60e3a380&FolderCTID=0x0120004BD02B53986692489994F371FDD6040E&id=%2Fsites%2FBaunasCloud%2FUPDevelopment%2FShared%20Documents%2FServices%2FArquitecturas%20de%20Referencia%2FBackend%2FJava&viewid=759458e1%2D6952%2D46af%2Db292%2Dfd52a1728c64)

# Tabla de contenidos
- [Especialización Backend](#especialización-backend)
- [Tabla de contenidos](#tabla-de-contenidos)
- [1. Arquitecturas de referencia](#1-arquitecturas-de-referencia)
- [2 . Estructura del proyecto](#2--estructura-del-proyecto)
- [3. Guia  para configurar el ambiente de desarrollo](#3-guia--para-configurar-el-ambiente-de-desarrollo)
  - [Windows](#windows)
- [4. Guia para clonar , ejecutar la plantilla y comenzar a desarrollar](#4-guia-para-clonar--ejecutar-la-plantilla-y-comenzar-a-desarrollar)
  - [4.1 Clonar el proyecto](#41-clonar-el-proyecto)
  - [4.2 Pre-ajustes (nombres, espacios de nombres, etc.)](#42-pre-ajustes-nombres-espacios-de-nombres-etc)
  - [4.3 Abrir el proyecto](#43-abrir-el-proyecto)
  - [4.4 Validación de JWT](#44-validación-de-jwt)
  - [4.5 Crear proyecto con el generador](#45-crear-proyecto-con-el-generador)
  - [4.6 Más información](#46-más-información)
- [5. Historial de Cambios](#5-historial-de-cambios)
- [6. ¿Cómo contribuyo?](#6-cómo-contribuyo)
    - [8. Agradecimientos](#8-agradecimientos)


# 1. Arquitecturas de referencia
Proyecto guia para todos los colaboradores en Baufest que utilicen java con Spring Boot


![Imagen de arquitecturas](/java.png "Diagrama arquitectura java ")


| Sección | Descripción                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| ------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1       | La solución está compuesta de una capa de servicios o APIs construida con Spring Boot utilizando el lenguaje Java diseñada bajo los preceptos y directrices de Clean Architecture, Domain-Driven Design y otros patrones de diseño y desarrollo.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| 2       | La capa de APIs sirve como punto de acceso a los servicios de la aplicación, exponiéndolos, usando Spring MVC. Aquí se encuentra las controladoras REST autodocumentadas bajo el estándar OpenAPI. Adicionalmente, se cuenta con middlewares con propósitos adicionales como por ejemplo la autenticación (utilizando Spring Security), autorización y healthchecks (utilizando Spring Boot Actuator). Esta aplicación usa la capa de Data Transfer Objects (DTOs) como los contratos para la entrada (Requests) y salida (Responses) de datos por parte de los servicios.Los servicios de la aplicación diseñados con la separación entre lecturas y escrituras distinguen claramente entre las operaciones que cambien los estados de las entidades de negocio (Commands) y los que consultan la información de estas (Queries). |
| 3       | El modelo de dominio representa el dominio de negocio sobre el cual se trabajan las operaciones de escritura y contiene las reglas de negocio, restricciones que se reflejan al modelar los requerimientos funcionales en entidades de dominio.Los comandos y eventos son consumidos para procesarse, estos se trabajan usando el modelo de dominio definido anteriormente. Los consumidores se encargan de publicar eventos para notificar de cambios en el estado de las entidades y otros consumidores puedan generar notificaciones para el usuario como la actualización de indicadores o el envío de notificaciones por correo.                                                                                                                                                                                              |
| 4       | El cambio en los estados de las entidades finalmente se apoya en la capa de Infraestructura la cual contiene la persistencia específica para los servicios de escritura (comandos. La implementación de esto se logra a través de un ORM, para este caso en particular es Spring Data JPA + Hibernate.La consulta de datos (búsquedas, listados y reportes) usa una implementación diferente a la de los comandos, optimizada para este propósito. Aquí se utiliza Spring JDBC Templates, que consulta vistas (u otra entidad desnormalizada) en la base de datos construidas para las necesidades específicas de la aplicación. Adicionalmente, aquí también se definen los contratos con servicios externos, tales como otras APIs REST utilizando librerías como Spring WebClient                                               |
| 5       | De forma adicional a las capas previamente mencionadas, se cuenta con un conjunto de librerías, frameworks y herramientas que proporcionan un soporte cross a la solución.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |

# 2 . Estructura del proyecto 
Este proyecto utiliza Clean Architecture con Spring Boot en Java.

```
 - application
     - aggregate
         BookAggregate.java
     - projection
         BookProjection.java
     - service
         + impl
         BookService
         CacheService
         SearchService
 - domain
     - command
         - book
             CreateBookCommand.java
     - constants
         EndpointConstants.java
     - DTO
         + Book
         + Nasa
    - EntityDomain
         BookDomain.java
    - queries
         - book
             BookByNameQuery.java
 - infrastructure
    - configuration
        JWTAuthorizationFilter.java
        OpenApiConfig.java
        SpringSecurityConfig.java
    - dataaccess
         + BookEntity
         + BookEntityMapper
         + NasaEntity
         + NasaEntityMapper
    - in
        - controller
            APINasaController.java
            BookController.java
            HelloWorldController.java
    - out
        - service
            - impl
            APINasaServices.java
    + repository
    - util
        Constants.java

```
1. [x] application/

   >Contiene las clases de entrada y salida de la aplicación que pasan a través de los controladores, así como los servicios de aplicacion y mappers (port out).

2. [x] domain/

   >Contiene la lógica de negocio independiente de la aplicación. Se encuentra en el centro de la arquitectura donde tenemos las entidades de la aplicación, que son las clases del modelo de la aplicación.

3. [x] infraestructure/

   >Contiene la mayoría de las dependencias de los recursos externos de la aplicación y que deben de ser implementadas en clases definidas en la capa de infraestructura.
   >Cuando hablamos de recursos externos nos referimos a implementaciones de acceso a bases de datos (ya sea con algún ORM u otra librería), implementaciones de envío de correos, uso de algún message broker o bus de conexión, etc.

# 3. Guia  para configurar el ambiente de desarrollo
## Windows
1. Instalar Choco (en Powershell)
```
Set-ExecutionPolicy Bypass -Scope Process -Force;
[System.Net.ServicePointManager]::SecurityProtocol =
[System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object
System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
```
2. Instalar Git
```
choco install -y git --force --force-dependencies
```
2. Instalar Java JDK 21
```
choco install -y openjdk --force --force-dependencies
```
3. Instalar Maven
```
choco install -y maven --force --force-dependencies
```
4. Instalar Intellij IDE
```
choco install -y intellijidea-community --force --force-dependencies
```

# 4. Guia para clonar , ejecutar la plantilla y comenzar a desarrollar

## 4.1 Clonar el proyecto
1. Ubicarse en la carpeta donde quiere copiar el proyecto
2. Abrir Git Bash
3. Ejecutar el comando


Por HTTPS
```
git clone https://gitlab.baufest.com/baufest-arquitecturas-de-referencia/java.git
```
Por SSH
```
git clone git@gitlab.baufest.com:baufest-arquitecturas-de-referencia/java.git
```
Para ssh usen esta guia https://www.youtube.com/watch?v=g0ZV-neSM7E

## 4.2 Pre-ajustes (nombres, espacios de nombres, etc.)

- Cambiar nombre de la carpeta principal "starter" al requerido en el nuevo proyecto.
- Cambiar nombre del paquete com.baufest.starter y sub paquetes al deseado para el proyecto, se recomienda el uso del IDE IntelliJ IDEA para factorizar el nombre, ya que realiza el cambio para toda ocurrencia dentro del proyecto.
- Eliminar dependencias maven no necesarias, por ejemplo h2 por sus limitantes en proyectos grandes.
- Eliminar la propiedad server.port en el archivo application.properties si desea utilizar el puerto por defecto en Spring boot, en caso contrario especificar el puerto requerido.
- Eliminar archivos donde se realizan las pruebas unitarias de ejemplo.
- Borrar archivos del caso de ejemplo, a excepción de los archivos que pueden ser reutilizables para cualquier otro proyecto: EndpointConstants, JWTAuthorizationFilter, OpenApiConfig, WebSecurityConfig, Constants, .gitignore, application.properties, CHANGELOG.md, CONTRIBUTING.md, pom.xml, README.md, etc.
- De los archivos anteriores se recomienda modificar su contenido de acuerdo al proyecto a desarrollar.

## 4.3 Abrir el proyecto 
1. Abrir Intellij IDE
2. File>Open> Buscar la carpeta..
3. Esperar a que descargue las librerias
4. Buscar *StarterApplication* y ejecutarlo(Run)
5. En el navegador entrar al swagger
```
http://localhost:8082/swagger-ui/index.html
```
## 4.4 Validación de JWT
1. Baufest ya cuenta con un generador de JWT mockeado y así poder realizar la validación del endpoint de hello a través siguiente repo: https://gitlab.baufest.com/baufest-arquitecturas-de-referencia/oauth2-mock-server-wrapper.
2. Para utilizar el generador se debe tener instalado [Node.js 20+](https://nodejs.org/en/). Opcionalmente, se recomienda tener instalado [Yarn 1.15.2+](https://classic.yarnpkg.com/lang/en/) (en pruebas con Fedora Linux no fue necesario tenerlo).
Luego en la raíz del proyecto se deben ejecutar los comandos **npm install** y **npm start**.
3. Luego de tener el generador corriendo por medio del endpoint http://localhost:8080/token se puede solicitar la generación de un token.
Para que esta solicitud sea aceptada se debe enviar en el body de la request en formato **json** lo siguiente:
{
    "user":"john",
    "password":"doe",
    "grant_type":"password"
}
4. Con el token generado como autenticador se puede utilizar el endpoint http://localhost:8082/hello para validar la autentificación del jwt.
- En el directorio [src/main/resources/Postman](src/main/resources/Postman) se encuentran 2 archivos de colecciones de postman que pueden ser importados.

## 4.5 Crear proyecto con el generador
1. Ingresar a [GitLab/access_token](https://gitlab.baufest.com/-/profile/personal_access_tokens) o seguir los pasos 2,3 y 4.

2. Ingresar a [Gitlab](https://gitlab.baufest.com)
   y en la esquina superior derecha, selecciona tu avatar.

   <img src="./paso2.PNG" alt="paso2" width="500">

3. Seleccione Edit profile.

   <img src="./paso3.PNG" alt="paso3" width="500">
  

4. En la barra lateral izquierda, seleccione Access Tokens .

    <img src="./paso4.PNG" alt="paso4" width="400">

5. Crear nuestro token con todos los permisos.

   <img src="./paso5.PNG" alt="paso5" width="500">

6. Copiar el token

   <img src="./paso6.PNG" alt="paso6" width="500">

7. Creamos el settings.xml dentro de user\.m2 con nuestro token
    ```
    <settings xmlns="http://maven.apache.org/SETTINGS/1.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"   
        xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 https://maven.apache.org/xsd/settings-1.0.0.xsd">    
        <localRepository>${user.home}/.m2/repository</localRepository>    
        <interactiveMode>true</interactiveMode>    
        <offline>false</offline>    
        <servers>        
            <server>            
                <id>gitlab-maven</id>            
                <configuration>                
                    <httpHeaders>                    
                        <property>                        
                            <name>NOMBRE TOKEN</name>                        
                            <value>AQUI-VA-TU-TOKEN</value>                    
                        </property>                
                    </httpHeaders>            
                </configuration>        
            </server>    
        </servers>    
        <profiles>        
            <profile>            
                <id>gitlab-maven</id>            
                <repositories>                
                    <repository>                    
                        <id>gitlab-maven</id>                    
                        <url>https://gitlab.baufest.com/api/v4/projects/2777/packages/maven</url>                
                    </repository>            
                </repositories>        
            </profile>    
        </profiles>    
        <activeProfiles>        
            <activeProfile>gitlab-maven</activeProfile>    
        </activeProfiles>
    </settings>
   ```
    Creamos el achetype-catalog.xml dentro de user\.m2\repository
    ```
    <?xml version="1.0" encoding="UTF-8"?>
   <archetype-catalog xsi:schemaLocation="http://maven.apache.org/plugins/maven-archetype-plugin/archetype-catalog/1.0.0 http://maven.apache.org/xsd/archetype-catalog-1.0.0.xsd"
   xmlns="http://maven.apache.org/plugins/maven-archetype-plugin/archetype-catalog/1.0.0"  
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">  
    <archetypes>
   <archetype>      
            <groupId>com.baufest</groupId>      
            <artifactId>starter-archetype</artifactId>      
            <version>0.0.1-SNAPSHOT</version>      
            <description>Baufest Starter remote archetype for Java</description>    
        </archetype>  
    </archetypes>
   </archetype-catalog>
    ```

8. Usando PowerShell descargamos el arquetipo en nuestra maquina local
    ```
    mvn dependency:get -Dartifact='com.baufest:starter-archetype:0.0.1-SNAPSHOT'
   ```
9. Usando PowerShell ejecutamos el siguiente comando para crear el proyecto.
    ```
    mvn archetype:generate -DarchetypeGroupId='com.baufest.arquitecturas.referencia'
   ```
   <img src="./paso9.PNG" alt="paso9" width="700">

## 4.6 Más información
- Tutorial de Sprint Boot: https://www.baeldung.com/spring-boot
- Clean Architecture con Spring Boot: https://www.baeldung.com/spring-boot-clean-architecture
- https://baufest.atlassian.net/wiki/spaces/BCJM/pages/704905902/Java


# 5. Historial de Cambios
- En el archivo [CHANGELOG.md](/CHANGELOG.md) (historial de cambios), se documentan todos los cambios notables del proyecto.
- El formato es basado en [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).


# 6. ¿Cómo contribuyo?
[CONTRIBUTING.md](/CONTRIBUTING.md)

### 8. Agradecimientos
Agradecimiento a los equipos: 

Arquitecturas Java BY23
- Jesus Angulo
- Luis Brandon Flores Gonzalez
- Leandro Alfonso
- Cesar Bismark Cainabel Lozada Madrid
- Alejandro Torres
- Marcelo Miño
- Celina Garcia Hernandez

=======

Arquitecturas Java BY22
- Ignacio Javier Kovacs
- Christian Rusterholz
- Luis Guzmán Peña
- Rodrigo Manuel Jorquera Pinto
- Nicolás Conde
- Hernán Nahuel Ladelfa
- Hernán Lavrencic
- Roger Cruz
- Diego Monje Gutierrez
- Marlon Leandro
- Edgar Chavez
- Alfredo Jimenez
- Alejandro Matute
- Israel Nizama

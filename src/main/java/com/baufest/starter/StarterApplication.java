package com.baufest.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>
 * Es fundamental para el boot de la aplicacion
 * </p>
 * 
 * @author nconde
 * @since 01/04/22
 */
@SpringBootApplication
public class StarterApplication {

	/**
	 * <p>
	 * Levanta el contexto de la app.
	 * </p>
	 * 
	 * @author nconde
	 * @since 01/04/22
	 */
	public static void main(String[] args) {
		SpringApplication.run(StarterApplication.class, args);
	}
}

package com.baufest.starter.application.service;

import com.baufest.starter.domain.DTO.Nasa.NasaResponseDTO;

/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
public interface CacheService {
	/**
	 * 
	 * @param q
	 * @param nasaResponseDTO
	 */

	void save(String q, NasaResponseDTO nasaResponseDTO);

	/**
	 * 
	 * @param q
	 * @return
	 */
	NasaResponseDTO get(String q);
}

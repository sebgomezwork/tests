package com.baufest.starter.application.service.impl;

import com.baufest.starter.infrastructure.dataaccess.NasaEntityMapper.CacheMapper;
import com.baufest.starter.application.service.CacheService;
import com.baufest.starter.infrastructure.dataaccess.NasaEntity.Cache;
import com.baufest.starter.infrastructure.repository.CacheRepository;
import com.baufest.starter.domain.DTO.Nasa.NasaResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import java.util.Optional;

@Service
/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
public class CacheServiceImpl implements CacheService {

    private final CacheRepository cacheRepository;

    @Autowired
    /**
     * 
     * @param cacheRepository
     */
    public CacheServiceImpl(CacheRepository cacheRepository) {
        this.cacheRepository = cacheRepository;
    }

    @Override
    @Transactional

    public void save(String q, NasaResponseDTO nasaResponseDTO) {
        this.cacheRepository.save(CacheMapper.convertToNasaLinks(q, nasaResponseDTO));
    }

    @Override
    public NasaResponseDTO get(String q) {
        Optional<Cache> cache = this.cacheRepository.findById(q);
        if(cache.isPresent()){
            return CacheMapper.convertToNasaLinks(cache.get());
        }
        return null;
    }
}

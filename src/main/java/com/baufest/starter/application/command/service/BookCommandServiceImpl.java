package com.baufest.starter.application.command.service;

import com.baufest.starter.domain.EntityDomain.BookDomain;
import com.baufest.starter.infrastructure.dataaccess.BookEntity.Book;
import com.baufest.starter.infrastructure.dataaccess.BookEntityMapper.BookMapper;
import com.baufest.starter.infrastructure.command.repository.BookCommandRepository;
import com.baufest.starter.domain.DTO.Book.BookDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
public class BookCommandServiceImpl implements BookCommandService {

    private BookCommandRepository bookCommandRepository;

    @Autowired
    /**
     * 
     * @param bookRepository
     */
    public BookCommandServiceImpl(BookCommandRepository bookCommandRepository) {
        this.bookCommandRepository = bookCommandRepository;
    }

    @Override
    public BookDTO save(BookDTO bookDTO){
        if(!BookDomain.toConvertDomain(bookDTO).hasValidNumberOfPages()){
            throw new IllegalArgumentException("No puede ingresar un libro con "+ bookDTO.getPages()+" paginas");
        }
        Book book = BookMapper.convertToEntity(bookDTO);
        return BookMapper.convertToDTO(bookCommandRepository.save(book));
    }

}

package com.baufest.starter.application.command.service;

import com.baufest.starter.domain.DTO.Book.BookDTO;

/**
 * <p>
 *
 * </p>
 * 
 * @author
 * @since 01/04/22
 */
public interface BookCommandService {
	/**
	 * 
	 * @param name
	 * @return
	 */

	BookDTO save(BookDTO bookDTO);
}

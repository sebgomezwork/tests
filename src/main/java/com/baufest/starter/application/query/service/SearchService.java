package com.baufest.starter.application.query.service;

import com.baufest.starter.domain.DTO.Nasa.NasaResponseDTO;
/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
public interface SearchService {
	/**
	 * 
	 * @param q
	 * @return
	 */
    NasaResponseDTO search(String q);
}

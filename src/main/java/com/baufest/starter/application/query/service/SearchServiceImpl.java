package com.baufest.starter.application.query.service;

import com.baufest.starter.application.service.CacheService;
import com.baufest.starter.domain.DTO.Nasa.NasaResponseDTO;
import com.baufest.starter.infrastructure.out.service.APINasaServices;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
public class SearchServiceImpl implements SearchService {

    private Log log = LogFactory.getLog(SearchServiceImpl.class);

    private final APINasaServices aPINasaServices;
    private final CacheService cacheService;

    @Autowired
    public SearchServiceImpl(APINasaServices aPINasaServices, CacheService cacheService) {
        this.aPINasaServices = aPINasaServices;
        this.cacheService = cacheService;
    }

    @Override
    public NasaResponseDTO search(String q) {

        log.info(String.format("Searching in cache q: %s", q));
        NasaResponseDTO response = this.cacheService.get(q);
        if(response != null){
            log.info(String.format("Cache hit q: %s", q));
            return response;
        }
        log.info(String.format("Cache miss q: %s", q));

        response = this.aPINasaServices.getImagesFrom(q);
        cacheService.save(q,response);
        return response;
    }

}

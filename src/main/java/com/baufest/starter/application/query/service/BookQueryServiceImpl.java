package com.baufest.starter.application.query.service;

import com.baufest.starter.domain.DTO.Book.BookDTO;
import com.baufest.starter.infrastructure.dataaccess.BookEntityMapper.BookMapper;
import com.baufest.starter.infrastructure.query.repository.BookQueryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
/**
 * <p>
 *
 * </p>
 *
 * @author
 * @since 01/04/22
 */
public class BookQueryServiceImpl implements BookQueryService {
    private BookQueryRepository bookRepository;

    @Autowired
    /**
     *
     * @param bookRepository
     */
    public BookQueryServiceImpl(BookQueryRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<BookDTO> findByName(String name) {
        return BookMapper.convertTo(bookRepository.findByName(name));
    }

}

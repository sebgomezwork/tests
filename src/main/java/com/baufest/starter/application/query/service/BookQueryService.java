package com.baufest.starter.application.query.service;

import com.baufest.starter.domain.DTO.Book.BookDTO;

import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author
 * @since 01/04/22
 */
public interface BookQueryService {
    /**
     * @param name
     * @return
     */

    List<BookDTO> findByName(String name);
}

package com.baufest.starter.domain.DTO.Nasa;

/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
public class NasaResponseDTO {
    private NasaCollectionDTO collection;

    public NasaCollectionDTO getCollection() {
        return collection;
    }

    public void setCollection(NasaCollectionDTO collection) {
        this.collection = collection;
    }

}

package com.baufest.starter.domain.DTO.Book;

/**
 * <p>
 * Transicion de BOOK
 * </p>
 * 
 * @author nconde
 * @since 01/04/22
 */
public class BookDTO {

    private Long number;
    private String name;
    private int pages;

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }


}

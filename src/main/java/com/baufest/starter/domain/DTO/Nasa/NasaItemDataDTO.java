package com.baufest.starter.domain.DTO.Nasa;

import com.baufest.starter.infrastructure.dataaccess.NasaEntity.ItemData;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.util.List;

/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class NasaItemDataDTO {

    private String center;
    private String title;
    private String nasaId;
    private String mediaType;
    private List<String> keywords;
    private String dateCreated;
    private String description;

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNasaId() {
        return nasaId;
    }

    public void setNasaId(String nasaId) {
        this.nasaId = nasaId;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static NasaItemDataDTO convertTo(ItemData itemData) {
        NasaItemDataDTO nasaItemDataDTO = new NasaItemDataDTO();
        nasaItemDataDTO.setDateCreated(itemData.getDateCreated());
        nasaItemDataDTO.setDescription(itemData.getDescription());
        nasaItemDataDTO.setCenter(itemData.getCenter());
        nasaItemDataDTO.setKeywords(itemData.getKeywords());
        nasaItemDataDTO.setMediaType(itemData.getMediaType());
        nasaItemDataDTO.setNasaId(itemData.getNasaId());
        nasaItemDataDTO.setTitle(itemData.getTitle());
        return nasaItemDataDTO;
    }
}

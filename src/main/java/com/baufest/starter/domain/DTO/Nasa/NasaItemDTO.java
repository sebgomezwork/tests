package com.baufest.starter.domain.DTO.Nasa;

import com.baufest.starter.infrastructure.dataaccess.NasaEntity.Item;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
public class NasaItemDTO {

    private String href;
    private List<NasaItemDataDTO> data;
    private List<NasaItemLinkDTO> links;


    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public List<NasaItemLinkDTO> getLinks() {
        return links;
    }

    public void setLinks(List<NasaItemLinkDTO> links) {
        this.links = links;
    }

    public List<NasaItemDataDTO> getData() {
        return data;
    }

    public void setData(List<NasaItemDataDTO> data) {
        this.data = data;
    }

    public static NasaItemDTO convertTo(Item item) {
        NasaItemDTO nasaItemDTO = new NasaItemDTO();
        nasaItemDTO.setHref(item.getHref());
        nasaItemDTO.setLinks(item.getItemLinks().stream().map(NasaItemLinkDTO::convertTo).collect(Collectors.toList()));
        nasaItemDTO.setData(item.getItemData().stream().map(NasaItemDataDTO::convertTo).collect(Collectors.toList()));
        return nasaItemDTO;
    }

}

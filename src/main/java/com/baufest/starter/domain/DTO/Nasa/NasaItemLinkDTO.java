package com.baufest.starter.domain.DTO.Nasa;

import com.baufest.starter.infrastructure.dataaccess.NasaEntity.ItemLink;

/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
public class NasaItemLinkDTO {

    private String rel;
    private String render;
    private String href;

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public String getRender() {
        return render;
    }

    public void setRender(String render) {
        this.render = render;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public static NasaItemLinkDTO convertTo(ItemLink itemLink) {
        NasaItemLinkDTO nasaItemLinkDTO = new NasaItemLinkDTO();
        nasaItemLinkDTO.setHref(itemLink.getHref());
        nasaItemLinkDTO.setRel(itemLink.getRel());
        nasaItemLinkDTO.setRender(itemLink.getRender());
        return nasaItemLinkDTO;
    }

}

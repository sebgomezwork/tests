package com.baufest.starter.domain.DTO.Nasa;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class NasaMetadataDTO {


    private Long totalHits;

    public Long getTotalHits() {
        return totalHits;
    }

    public void setTotalHits(Long totalHits) {
        this.totalHits = totalHits;
    }

}

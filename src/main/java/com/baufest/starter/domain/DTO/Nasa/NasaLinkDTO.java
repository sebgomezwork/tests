package com.baufest.starter.domain.DTO.Nasa;

import com.baufest.starter.infrastructure.dataaccess.NasaEntity.Link;

/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
public class NasaLinkDTO {

    private String rel;
    private String prompt;
    private String href;

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public static NasaLinkDTO convertTo(Link link) {
        NasaLinkDTO nasaLinkDTO = new NasaLinkDTO();
        nasaLinkDTO.setHref(link.getHref());
        nasaLinkDTO.setPrompt(link.getPrompt());
        nasaLinkDTO.setRel(link.getRel());
        return nasaLinkDTO;
    }
}

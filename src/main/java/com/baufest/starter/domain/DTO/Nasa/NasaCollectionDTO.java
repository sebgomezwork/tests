package com.baufest.starter.domain.DTO.Nasa;

import java.util.List;

/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
public class NasaCollectionDTO {

    private String version;
    private String href;
    private List<NasaItemDTO> items;
    private NasaMetadataDTO metadata;
    private List<NasaLinkDTO> links;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public List<NasaItemDTO> getItems() {
        return items;
    }

    public void setItems(List<NasaItemDTO> items) {
        this.items = items;
    }

    public NasaMetadataDTO getMetadata() {
        return metadata;
    }

    public void setMetadata(NasaMetadataDTO metadata) {
        this.metadata = metadata;
    }

    public List<NasaLinkDTO> getLinks() {
        return links;
    }

    public void setLinks(List<NasaLinkDTO> links) {
        this.links = links;
    }

}

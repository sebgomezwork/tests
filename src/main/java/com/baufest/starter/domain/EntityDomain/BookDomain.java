package com.baufest.starter.domain.EntityDomain;

import com.baufest.starter.domain.DTO.Book.BookDTO;

public class BookDomain {
    private String name;
    private int pages;

    public boolean hasValidNumberOfPages(){
        return this.pages > 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public static BookDomain toConvertDomain(BookDTO bookDTO){
        BookDomain bookDomain = new BookDomain();
        bookDomain.setName(bookDTO.getName());
        bookDomain.setPages(bookDTO.getPages());
        return bookDomain;
    }
}

package com.baufest.starter.domain.constants;

public final class EndpointConstants {
    public static final String BOOK_PATH = "/v1";
    public static final String BOOK_ENDPOINT = "/books/{name}";
    public static final String BOOK_PATH_VARIABLE = "name";
    public static final String BOOK_POST_ENDPOINT = "/books";

    public static final String API_NASA_ENPOINT_SEARCH = "/search";
    public static final String API_NASA_QUERY_PARAM = "q";

}

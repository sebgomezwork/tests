package com.baufest.starter.infrastructure.command.controller;

import com.baufest.starter.application.command.service.BookCommandService;
import com.baufest.starter.domain.constants.EndpointConstants;
import com.baufest.starter.domain.DTO.Book.BookDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *
 * </p>
 * 
 * @author
 * @since 01/04/22
 */
@RestController
@RequestMapping(EndpointConstants.BOOK_PATH)
public class BookController {

	private BookCommandService bookCommandService;

	/**
	 * 
	 * @param bookCommandService
	 */
	@Autowired
	public BookController(BookCommandService bookCommandService) {
		this.bookCommandService = bookCommandService;
	}

	/**
	 * 
	 * @param bookName
	 * @return
	 */

	@PostMapping(value = EndpointConstants.BOOK_POST_ENDPOINT)
	public ResponseEntity<?> saveBook(@RequestBody BookDTO bookDTO) {
		BookDTO bookDTO1 = new BookDTO();
		try {
			bookDTO1 = bookCommandService.save(bookDTO);
		} catch (IllegalArgumentException ex) {
			Map<String, Object> responseError = new HashMap<>();
			responseError.put("Mensaje ", "Ocurrio un error al persistir el registro de libro");
			responseError.put("Error ", ex.getMessage());
			return new ResponseEntity<Map<String, Object>>(responseError, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<BookDTO>(bookDTO1, HttpStatus.CREATED);
	}

}

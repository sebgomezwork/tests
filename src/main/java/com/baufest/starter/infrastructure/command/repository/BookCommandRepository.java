package com.baufest.starter.infrastructure.command.repository;

import com.baufest.starter.infrastructure.dataaccess.BookEntity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
@Repository
public interface BookCommandRepository extends JpaRepository<Book, Long> {
	/**
	 * 
	 * @param name
	 * @return
	 */
}

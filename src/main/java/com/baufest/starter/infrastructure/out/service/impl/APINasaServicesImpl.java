package com.baufest.starter.infrastructure.out.service.impl;

import com.baufest.starter.domain.DTO.Nasa.NasaResponseDTO;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.baufest.starter.infrastructure.out.service.APINasaServices;
import com.baufest.starter.infrastructure.util.Constants;


/**
 * <p></p>
 *
 * @return
 * @author nconde
 * @since 29/04/2022
 */
@Service
public class APINasaServicesImpl implements APINasaServices{

	/**
	 * <p>Access the NASA Image and Video Library site at images.nasa.gov</p>
	 *
	 * @see <a href="https://images.nasa.gov/docs/images.nasa.gov_api_docs.pdf">
	 *     NASA Image and Video Library API documentation.</a>
	 * @return
	 * @author nconde
	 * @since 29/04/2022
	 */
	public NasaResponseDTO getImagesFrom(String q) {
		WebClient client = WebClient.create(Constants.API_NASA_PATH_BASE)
				          .mutate().codecs(configure -> configure
						  .defaultCodecs().maxInMemorySize(16 * 1024 * 1024))
				          .build();

		WebClient.ResponseSpec responseSpec = client.get().uri(
				uriBuilder -> uriBuilder
				.path(Constants.API_NASA_PATH_SEARCH)
			    .queryParam(Constants.API_NASA_PARAM_Q, q)
			    .build())
			  .retrieve();

		NasaResponseDTO responseBody = responseSpec.bodyToMono(NasaResponseDTO.class).block();


		return responseBody;
	}

}

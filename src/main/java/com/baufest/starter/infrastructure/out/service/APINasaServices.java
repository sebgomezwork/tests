package com.baufest.starter.infrastructure.out.service;

import com.baufest.starter.domain.DTO.Nasa.NasaResponseDTO;

/**
 * <p></p>
 * 
 * @return
 * @author nconde
 * @since 29/04/2022
 */
public interface APINasaServices {
	
	/**
	 * <p></p>
	 * 
	 * @return
	 * @author nconde
	 * @since 29/04/2022
	 */
	public NasaResponseDTO getImagesFrom(String q);

}

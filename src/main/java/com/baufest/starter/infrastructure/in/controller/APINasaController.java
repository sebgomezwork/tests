package com.baufest.starter.infrastructure.in.controller;

import com.baufest.starter.application.query.service.SearchService;
import com.baufest.starter.domain.DTO.Nasa.NasaResponseDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.baufest.starter.domain.constants.EndpointConstants;

/**
 * <p></p>
 *
 * @return
 * @author nconde
 * @since 29/04/2022
 */
@RestController
@RequestMapping(EndpointConstants.BOOK_PATH)
public class APINasaController {

	private final SearchService searchService;

	@Autowired
	public APINasaController(SearchService searchService) {
		this.searchService = searchService;
	}


	/**
	 * <p></p>
	 *
	 * @return
	 * @author nconde
	 * @since 29/04/2022
	 */

	@Operation(summary = "Use this API to access the NASA Image and Video Library site at images.nasa.gov")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Everything worked as expected."),
			@ApiResponse(responseCode = "400", description = "The request was unacceptable", content = {
					@Content(schema = @Schema(hidden = true))
			}),
			@ApiResponse(responseCode = "500, 502, 503, 504", description = "Something went wrong on the API’s end",
					content = {@Content(schema = @Schema(hidden = true))}
			)
	})
	@GetMapping(value = EndpointConstants.API_NASA_ENPOINT_SEARCH)
	public ResponseEntity<NasaResponseDTO> search(
			@Parameter(description = "Free text search terms to compare to all indexed metadata", example = "apollo 11")
			@RequestParam(value =  EndpointConstants.API_NASA_QUERY_PARAM) String q) {
		return ResponseEntity.ok(this.searchService.search(q));
	}

}
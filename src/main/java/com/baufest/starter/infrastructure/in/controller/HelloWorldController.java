package com.baufest.starter.infrastructure.in.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *
 * </p>
 * 
 * @author
 * @since 01/04/22
 */
@RestController
public class HelloWorldController {

	@Operation(summary = "Endpoint que verifica la autentificacion de un usuario y retorna un String con el nombre  e id del usuario en caso de ser validado", security = @SecurityRequirement(name = "bearerAuth"))
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Operacion exitosa"),
			@ApiResponse(responseCode = "403", description  = "Header de autentificacion invalido"),
			@ApiResponse(responseCode = "500", description  = "Token invalida"),
	})
	@GetMapping("hello")
	public String helloWorld(@RequestParam(value = "name", defaultValue = "World") String name) {
		return "Hello " + name + "!! Your user id is: "
				+ SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}


}
package com.baufest.starter.infrastructure.dataaccess.NasaEntity;

import com.baufest.starter.domain.DTO.Nasa.NasaItemDataDTO;

import jakarta.persistence.*;
import java.util.List;

/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
@Entity
public class ItemData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    private String center;
    private String title;
    private String nasaId;
    private String mediaType;

    @ElementCollection
    @Column(length = 600)
    private List<String> keywords;

    private String dateCreated;

    @Column(length = 80000)
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNasaId() {
        return nasaId;
    }

    public void setNasaId(String nasaId) {
        this.nasaId = nasaId;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static ItemData convertTo(NasaItemDataDTO nasaItemDataDTO) {
        ItemData itemData = new ItemData();
        itemData.setDateCreated(nasaItemDataDTO.getDateCreated());
        itemData.setDescription(nasaItemDataDTO.getDescription());
        itemData.setCenter(nasaItemDataDTO.getCenter());
        itemData.setKeywords(nasaItemDataDTO.getKeywords());
        itemData.setMediaType(nasaItemDataDTO.getMediaType());
        itemData.setNasaId(nasaItemDataDTO.getNasaId());
        itemData.setTitle(nasaItemDataDTO.getTitle());
        return itemData;
    }
}

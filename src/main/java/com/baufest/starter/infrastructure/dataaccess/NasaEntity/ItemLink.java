package com.baufest.starter.infrastructure.dataaccess.NasaEntity;

import com.baufest.starter.domain.DTO.Nasa.NasaItemLinkDTO;

import jakarta.persistence.*;

/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
@Entity
public class ItemLink {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    private String rel;
    private String render;
    @Column(length = 300)
    private String href;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public String getRender() {
        return render;
    }

    public void setRender(String render) {
        this.render = render;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public static ItemLink convertTo(NasaItemLinkDTO nasaItemLinkDTO) {
        ItemLink itemLink = new ItemLink();
        itemLink.setHref(nasaItemLinkDTO.getHref());
        itemLink.setRender(nasaItemLinkDTO.getRender());
        itemLink.setRel(nasaItemLinkDTO.getRel());
        return itemLink;
    }
}

package com.baufest.starter.infrastructure.dataaccess.NasaEntity;

import com.baufest.starter.domain.DTO.Nasa.NasaItemDTO;

import jakarta.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
@Entity
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    private String href;

    @OneToMany(cascade = {CascadeType.ALL})
    private List<ItemLink> itemLinks;

    @OneToMany(cascade = {CascadeType.ALL})
    private List<ItemData> itemData;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public List<ItemLink> getItemLinks() {
        return itemLinks;
    }

    public void setItemLinks(List<ItemLink> itemLinks) {
        this.itemLinks = itemLinks;
    }

    public List<ItemData> getItemData() {
        return itemData;
    }

    public void setItemData(List<ItemData> itemData) {
        this.itemData = itemData;
    }

    public static Item convertTo(NasaItemDTO nasaItemDTO) {
        Item item = new Item();
        item.setHref(nasaItemDTO.getHref());
        if(nasaItemDTO != null && nasaItemDTO.getLinks() != null) {
            item.setItemLinks(nasaItemDTO.getLinks().stream().map(ItemLink::convertTo).collect(Collectors.toList()));
        }
        if(!nasaItemDTO.getData().isEmpty()){
            item.setItemData(nasaItemDTO.getData().stream().map(ItemData::convertTo).collect(Collectors.toList()));
        }
        return item;
    }


}

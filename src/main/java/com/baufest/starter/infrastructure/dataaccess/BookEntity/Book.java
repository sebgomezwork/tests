package com.baufest.starter.infrastructure.dataaccess.BookEntity;

import jakarta.persistence.*;

/**
 * <p>
 * Entidad Book utiliza como ejemplo
 * </p>
 * 
 * @author nconde
 * @since 01/04/22
 */
@Entity
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private int pages;

    public Book() {
    }

    public Book(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int numberOfPages) {
        this.pages = numberOfPages;
    }
}

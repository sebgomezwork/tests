package com.baufest.starter.infrastructure.dataaccess.NasaEntity;

import jakarta.persistence.*;

/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
@Entity
public class Cache {

    @Id
    @Column(name = "id", nullable = false)
    private String id;

    @OneToOne(cascade = {CascadeType.ALL})
    private Collection collection;

    public Collection getCollection() {
        return collection;
    }

    public void setCollection(Collection collection) {
        this.collection = collection;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}

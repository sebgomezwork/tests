package com.baufest.starter.infrastructure.dataaccess.NasaEntity;

import jakarta.persistence.*;
import java.util.List;

/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
@Entity
public class Collection {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String version;
    private String href;

    @OneToOne(cascade = {CascadeType.ALL})
    private Metadata metadata;

    @OneToMany(cascade = {CascadeType.ALL})
    private List<Link> links;

    @OneToMany(cascade = {CascadeType.ALL})
    private List<Item> items;

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

}

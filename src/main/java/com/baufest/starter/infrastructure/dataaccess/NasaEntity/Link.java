package com.baufest.starter.infrastructure.dataaccess.NasaEntity;

import com.baufest.starter.domain.DTO.Nasa.NasaLinkDTO;

import jakarta.persistence.*;

/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
@Entity
public class Link {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    private String rel;
    private String prompt;
    private String href;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public static Link convertTo(NasaLinkDTO nasaLinkDTO) {
        Link link = new Link();
        link.setHref(nasaLinkDTO.getHref());
        link.setPrompt(nasaLinkDTO.getPrompt());
        link.setRel(nasaLinkDTO.getRel());
        return link;
    }
}

package com.baufest.starter.infrastructure.dataaccess.BookEntityMapper;

import com.baufest.starter.infrastructure.dataaccess.BookEntity.Book;
import com.baufest.starter.domain.DTO.Book.BookDTO;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * Mapper para convertir <b>Books</b>
 * </p>
 * 
 * @author nconde
 * @since 01/04/22
 */
public final class BookMapper {

	/**
	 * <p>
	 * convierte <b>Books</b> en <b>BookDTO</b>
	 * </p>
	 * 
	 * @author nconde
	 * @since 01/04/22
	 */

	public static List<BookDTO> convertTo(List<Book> books) {
		return books.stream().map(BookMapper::convertToDTO).collect(Collectors.toList());
	}

	public static Book convertToEntity(BookDTO bookDTO){
		if(bookDTO == null){
			return null;
		}
		Book book = new Book();
		book.setId(bookDTO.getNumber());
		book.setName(bookDTO.getName());
		book.setPages(bookDTO.getPages());
		return book;
	}

	public static BookDTO convertToDTO(Book book){
		if(book == null){
			return null;
		}
		BookDTO bookDTO = new BookDTO();
		bookDTO.setNumber(book.getId());
		bookDTO.setName(book.getName());
		bookDTO.setPages(book.getPages());
		return bookDTO;
	}
}

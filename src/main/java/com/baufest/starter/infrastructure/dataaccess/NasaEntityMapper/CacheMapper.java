package com.baufest.starter.infrastructure.dataaccess.NasaEntityMapper;

import com.baufest.starter.domain.DTO.Nasa.*;
import com.baufest.starter.infrastructure.dataaccess.NasaEntity.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * Mapper para convertir Resultado
 * </p>
 * 
 * @author
 * @since 01/04/22
 */
public class CacheMapper {

	/**
	 * <p>
	 * Convierte el link de nasa a un cache
	 * </p>
	 * 
	 * @param q
	 * @param nasaResponseDTO
	 * @return
	 */
	public static Cache convertToNasaLinks(String q, NasaResponseDTO nasaResponseDTO) {
		Cache cache = new Cache();
		cache.setId(q);
		cache.setCollection(convertToNasaLinks(nasaResponseDTO.getCollection()));
		return cache;
	}

	/**
	 * <p>
	 * Convierte el lñinks a NasaResponse
	 * </p>
	 * 
	 * @param cache
	 * @return
	 */
	public static NasaResponseDTO convertToNasaLinks(Cache cache) {
		NasaResponseDTO nasaResponseDTO = new NasaResponseDTO();
		nasaResponseDTO.setCollection(convertToNasaLinks(cache.getCollection()));
		return nasaResponseDTO;
	}

	/**
	 * 
	 * @param nasaCollectionDTO
	 * @return
	 */
	private static Collection convertToNasaLinks(NasaCollectionDTO nasaCollectionDTO) {
		Collection collection = new Collection();
		collection.setVersion(nasaCollectionDTO.getVersion());
		collection.setHref(nasaCollectionDTO.getHref());
		collection.setMetadata(convertTo(nasaCollectionDTO.getMetadata()));
		collection.setLinks(convertToLinks(nasaCollectionDTO.getLinks()));
		collection.setItems(covertToItems(nasaCollectionDTO.getItems()));
		return collection;
	}

	/**
	 * 
	 * @param nasaItemsDTO
	 * @return
	 */
	private static List<Item> covertToItems(List<NasaItemDTO> nasaItemsDTO) {
		return nasaItemsDTO.stream().map(Item::convertTo).collect(Collectors.toList());
	}

	/**
	 * 
	 * @param nasaLinksDTO
	 * @return
	 */
	private static List<Link> convertToLinks(List<NasaLinkDTO> nasaLinksDTO) {
		List<Link> links = null;
		if (nasaLinksDTO != null) {
			links = nasaLinksDTO.stream().map(Link::convertTo).collect(Collectors.toList());
		}
		return links;
	}

	/**
	 * 
	 * @param nasaMetadataDTO
	 * @return
	 */
	private static Metadata convertTo(NasaMetadataDTO nasaMetadataDTO) {
		Metadata metadata = new Metadata();
		metadata.setTotalHits(nasaMetadataDTO.getTotalHits());
		return metadata;
	}

	/**
	 * 
	 * @param collection
	 * @return
	 */
	private static NasaCollectionDTO convertToNasaLinks(Collection collection) {
		NasaCollectionDTO nasaCollectionDTO = new NasaCollectionDTO();
		nasaCollectionDTO.setHref(collection.getHref());
		nasaCollectionDTO.setVersion(collection.getVersion());
		nasaCollectionDTO.setMetadata(convertTo(collection.getMetadata()));
		nasaCollectionDTO.setLinks(convertToNasaLinks(collection.getLinks()));
		nasaCollectionDTO.setItems(convertToNasaItems(collection.getItems()));
		return nasaCollectionDTO;
	}

	/**
	 * 
	 * @param items
	 * @return
	 */
	private static List<NasaItemDTO> convertToNasaItems(List<Item> items) {
		return items.stream().map(NasaItemDTO::convertTo).collect(Collectors.toList());
	}

	/**
	 * 
	 * @param links
	 * @return
	 */
	private static List<NasaLinkDTO> convertToNasaLinks(List<Link> links) {
		return links.stream().map(NasaLinkDTO::convertTo).collect(Collectors.toList());
	}

	/**
	 * 
	 * @param metadata
	 * @return
	 */
	private static NasaMetadataDTO convertTo(Metadata metadata) {
		NasaMetadataDTO nasaMetadataDTO = new NasaMetadataDTO();
		nasaMetadataDTO.setTotalHits(metadata.getTotalHits());
		return nasaMetadataDTO;
	}

}

package com.baufest.starter.infrastructure.repository;

import com.baufest.starter.infrastructure.dataaccess.NasaEntity.Cache;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
public interface CacheRepository extends JpaRepository<Cache, String> {
}

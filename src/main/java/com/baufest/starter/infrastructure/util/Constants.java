package com.baufest.starter.infrastructure.util;


/**
 * <p></p>
 * 
 * @return
 * @author nconde
 * @since 29/04/2022
 */

public class Constants {
	
	public static final String API_NASA_PATH_BASE = "https://images-api.nasa.gov";
	
	public static final String API_NASA_PATH_SEARCH = "/search";
	
	public static final String API_NASA_PARAM_Q = "q";

}

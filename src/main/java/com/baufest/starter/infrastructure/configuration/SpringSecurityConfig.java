package com.baufest.starter.infrastructure.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static org.springframework.security.web.util.matcher.AntPathRequestMatcher.antMatcher;

/**
 * <p>
 *
 * </p>
 * 
 * @author
 * @since 01/04/22
 */

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig {
	
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		http.csrf(config -> config.disable());
		
		http.addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
		
		http.authorizeHttpRequests(config -> config
				.requestMatchers(antMatcher("/hello")).authenticated()
				.requestMatchers(
						antMatcher("/v1/**"),
						antMatcher("/v3/api-docs/**"),
						antMatcher("/swagger-ui/**")).permitAll()
				.anyRequest().denyAll()
				);
		
		http.headers(config -> config
				.frameOptions(subconfig -> subconfig.disable()));
		
		return http.build();
	}
}

package com.baufest.starter.infrastructure.configuration;

import java.io.IOException;
import java.net.MalformedURLException;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkException;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.JwkProviderBuilder;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * <p>
 *
 * </p>
 * 
 * @author
 * @since 01/04/22
 */
public class JWTAuthorizationFilter extends OncePerRequestFilter {
	private Log log = LogFactory.getLog(JWTAuthorizationFilter.class);

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		try {
			if (existeJWTToken(request)) {
				DecodedJWT jwt = validateToken(request);
				setUpSpringAuthentication(jwt);
			} else {
				log.warn(
						"No authorization header. Request could be denied with error 403 if there is no explicit rule to allow it.");
				SecurityContextHolder.clearContext();
			}
			chain.doFilter(request, response);
		} catch (JwkException e) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			response.sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
			log.error(e.getMessage());
		}
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws IllegalArgumentException
	 * @throws JwkException
	 * @throws MalformedURLException
	 */
	private DecodedJWT validateToken(HttpServletRequest request)
			throws IllegalArgumentException, JwkException, MalformedURLException {
		String jwtToken = request.getHeader("Authorization").replace("Bearer ", "");
		DecodedJWT jwt = JWT.decode(jwtToken);
		java.net.URL url = new java.net.URL(jwt.getIssuer().replace("10.0.2.2", "localhost") + "/jwks"); // workaround
																											// the
																											// hardcoded
																											// ip for
																											// the
																											// emulator...
		JwkProvider provider = new JwkProviderBuilder(url).cached(10, 24, TimeUnit.HOURS)
				.rateLimited(10, 1, TimeUnit.MINUTES).build();
		Jwk jwk = provider.get(jwt.getKeyId());
		Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) jwk.getPublicKey(), null);
		algorithm.verify(jwt);
		if (jwt.getExpiresAt().before(Calendar.getInstance().getTime())) {
			throw new JwkException("Expired token!");
		}
		return jwt;
	}

	/**
	 * 
	 * @param jwt
	 */
	private void setUpSpringAuthentication(DecodedJWT jwt) {
		// here you can call the DB using the jwt.getSubject() to validate the user and
		// get the roles...
		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority("admin"));
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(jwt.getSubject(), null,
				authorities);
		SecurityContextHolder.getContext().setAuthentication(auth);
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	private boolean existeJWTToken(HttpServletRequest request) {
		String authenticationHeader = request.getHeader("Authorization");
		return !(authenticationHeader == null || !authenticationHeader.startsWith("Bearer "));
	}

}

package com.baufest.starter.infrastructure.query.controller;

import com.baufest.starter.application.query.service.BookQueryService;
import com.baufest.starter.domain.DTO.Book.BookDTO;
import com.baufest.starter.domain.constants.EndpointConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *
 * </p>
 * 
 * @author
 * @since 01/04/22
 */
@RestController
@RequestMapping(value = EndpointConstants.BOOK_PATH)
public class BookQueryController {

	private BookQueryService bookQueryService;

	/**
	 *
	 * @param bookQueryService
	 */
	@Autowired
	public BookQueryController(BookQueryService bookQueryService) {
		this.bookQueryService = bookQueryService;
	}

	@GetMapping(value = EndpointConstants.BOOK_ENDPOINT)
	public ResponseEntity<List<BookDTO>> getBooks(
			@PathVariable(name = EndpointConstants.BOOK_PATH_VARIABLE) String bookName) {
		return ResponseEntity.ok(bookQueryService.findByName(bookName));
	}

}

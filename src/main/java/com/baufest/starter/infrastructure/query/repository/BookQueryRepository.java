package com.baufest.starter.infrastructure.query.repository;

import com.baufest.starter.infrastructure.dataaccess.BookEntity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *
 * </p>
 * 
 * @author 
 * @since 01/04/22
 */
@Repository
public interface BookQueryRepository extends JpaRepository<Book, Long> {
	/**
	 * 
	 * @param name
	 * @return
	 */
	List<Book> findByName(String name);
}

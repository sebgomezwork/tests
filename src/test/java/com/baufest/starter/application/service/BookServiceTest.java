package com.baufest.starter.application.service;

import com.baufest.starter.application.command.service.BookCommandServiceImpl;
import com.baufest.starter.domain.DTO.Book.BookDTO;
import com.baufest.starter.infrastructure.command.repository.BookCommandRepository;
import com.baufest.starter.infrastructure.dataaccess.BookEntity.Book;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpServerErrorException;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class BookServiceTest {

    @InjectMocks
    BookCommandServiceImpl bookService;

    @Mock
    private BookCommandRepository bookRepository;

    BookDTO book = new BookDTO();
    Book bookEntity = new Book();

    @BeforeEach
    void setup(){
        book.setName("Example");
        book.setNumber(12L);
        book.setPages(10);

        bookEntity.setId(12L);
        bookEntity.setName("Example");
        bookEntity.setPages(10);
    }

    @Test
    void test_OK_save(){
        Mockito.when(this.bookRepository.save(Mockito.any(Book.class))).thenReturn(bookEntity);
        BookDTO response = bookService.save(this.book);
        Assertions.assertEquals(this.book.getName(),response.getName());
    }


    @Test
    void test_save_ERROR_PAGE(){
        book.setPages(0);
        String error = "No puede ingresar un libro con " + this.book.getPages() + " paginas";

        Mockito.when(this.bookRepository.save(Mockito.any(Book.class))).thenThrow(new IllegalArgumentException(error));
        Assertions.assertThrows(IllegalArgumentException.class, () -> bookService.save(this.book), error);
    }

    @Test
    void test_save_ERROR_REPOSITORY(){
        HttpStatus error = HttpStatus.INTERNAL_SERVER_ERROR;
        Mockito.when(this.bookRepository.save(Mockito.any(Book.class))).thenThrow(new HttpServerErrorException(error));
        Assertions.assertThrows(HttpServerErrorException.class, () -> bookService.save(this.book), error.getReasonPhrase());
    }

}
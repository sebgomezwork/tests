package com.baufest.starter.application.service;

import com.baufest.starter.application.query.service.SearchService;
import com.baufest.starter.application.service.impl.CacheServiceImpl;
import com.baufest.starter.application.query.service.SearchServiceImpl;
import com.baufest.starter.domain.DTO.Nasa.*;
import com.baufest.starter.domain.constants.Constants;
import com.baufest.starter.infrastructure.dataaccess.NasaEntity.*;
import com.baufest.starter.infrastructure.repository.CacheRepository;
import com.baufest.starter.infrastructure.out.service.APINasaServices;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Optional;

/**
 * <p>
 *
 * </p>
 * 
 * @author
 * @since 01/04/22
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { SearchServiceImpl.class, CacheServiceImpl.class })
public class SearchServiceTest {

	@Autowired
	private SearchService searchService;

	@MockBean
	private CacheRepository cacheRepository;

	@MockBean
	private APINasaServices aPINasaServices;

	/**
	 * 
	 */
	@Test
	public void searchImagesSuccessfully() {
		Mockito.when(this.aPINasaServices.getImagesFrom(Constants.Q_PARAM)).thenReturn(this.createApiNasaResponse());
		NasaResponseDTO response = searchService.search(Constants.Q_PARAM);
		Assertions.assertEquals(Constants.SEARCH_NASA_RESPONSE_VERSION, response.getCollection().getVersion());
		Assertions.assertEquals(Constants.SEARCH_NASA_RESPONSE_HREF, response.getCollection().getHref());
	}

	/**
	 * 
	 */
	@Test
	public void searchImagesCacheHit() {
		Mockito.when(this.cacheRepository.findById(Constants.Q_PARAM)).thenReturn(this.createCacheResponse());
		NasaResponseDTO response = searchService.search(Constants.Q_PARAM);
		Assertions.assertEquals(Constants.SEARCH_NASA_RESPONSE_VERSION, response.getCollection().getVersion());
		Assertions.assertEquals(Constants.SEARCH_NASA_RESPONSE_HREF, response.getCollection().getHref());
	}

	/**
	 * 
	 * @return
	 */
	private NasaResponseDTO createApiNasaResponse() {
		NasaResponseDTO response = new NasaResponseDTO();
		response.setCollection(new NasaCollectionDTO());
		response.getCollection().setVersion(Constants.SEARCH_NASA_RESPONSE_VERSION);
		response.getCollection().setHref(Constants.SEARCH_NASA_RESPONSE_HREF);
		response.getCollection().setMetadata(new NasaMetadataDTO());
		response.getCollection().setItems(new ArrayList<>());
		NasaItemDTO nasaItemDTO = new NasaItemDTO();
		nasaItemDTO.setHref(Constants.SEARCH_NASA_RESPONSE_HREF);
		nasaItemDTO.setData(new ArrayList<>());
		NasaItemDataDTO nasaItemDataDTO = new NasaItemDataDTO();
		nasaItemDTO.getData().add(nasaItemDataDTO);
		nasaItemDTO.setLinks(new ArrayList<>());
		NasaItemLinkDTO nasaItemLinkDTO = new NasaItemLinkDTO();
		nasaItemDTO.getLinks().add(nasaItemLinkDTO);
		response.getCollection().getItems().add(nasaItemDTO);
		response.getCollection().setLinks(new ArrayList<>());
		NasaLinkDTO nasaLinkDTO = new NasaLinkDTO();
		response.getCollection().getLinks().add(nasaLinkDTO);
		return response;
	}

	/**
	 * 
	 * @return
	 */
	private Optional<Cache> createCacheResponse() {
		Long id = 1L;
		Cache cache = new Cache();
		cache.setId(Constants.Q_PARAM);
		cache.setCollection(new Collection());
		cache.getCollection().setId(id);
		cache.getCollection().setVersion(Constants.SEARCH_NASA_RESPONSE_VERSION);
		cache.getCollection().setHref(Constants.SEARCH_NASA_RESPONSE_HREF);
		cache.getCollection().setMetadata(new Metadata());
		cache.getCollection().getMetadata().setId(id);
		cache.getCollection().getMetadata().setTotalHits(10000L);
		cache.getCollection().setItems(new ArrayList<>());
		Item item = new Item();
		item.setId(id);
		item.setHref(Constants.SEARCH_NASA_RESPONSE_HREF);
		item.setItemData(new ArrayList<>());
		ItemData itemData = new ItemData();
		itemData.setId(id);
		itemData.setKeywords(new ArrayList<>());
		item.getItemData().add(itemData);
		item.setItemLinks(new ArrayList<>());
		ItemLink itemLink = new ItemLink();
		itemLink.setId(id);
		item.getItemLinks().add(itemLink);
		cache.getCollection().getItems().add(item);
		cache.getCollection().setLinks(new ArrayList<>());
		Link link = new Link();
		link.setId(id);
		link.setHref(Constants.SEARCH_NASA_RESPONSE_HREF);
		cache.getCollection().getLinks().add(link);
		return Optional.of(cache);
	}

}

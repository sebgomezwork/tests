package com.baufest.starter.infrastructure.in.controller;

import com.baufest.starter.application.query.service.SearchService;
import com.baufest.starter.domain.constants.Constants;
import com.baufest.starter.domain.constants.EndpointConstants;
import com.baufest.starter.infrastructure.configuration.SpringSecurityConfig;
import com.baufest.starter.domain.DTO.Nasa.NasaCollectionDTO;
import com.baufest.starter.domain.DTO.Nasa.NasaResponseDTO;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * <p>
 *
 * </p>
 * 
 * @author
 * @since 01/04/22
 */
@WebMvcTest(controllers = APINasaController.class,
		includeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE,
				classes = SpringSecurityConfig.class))
public class APINasaControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private SearchService searchService;

	/**
	 * 
	 * @throws Exception
	 */
	@Test
	public void search() throws Exception {

		Mockito.when(this.searchService.search(Constants.Q_PARAM)).thenReturn(this.createSearchServiceResponse());

		this.mockMvc
				.perform(MockMvcRequestBuilders
						.get(EndpointConstants.BOOK_PATH.concat(EndpointConstants.API_NASA_ENPOINT_SEARCH))
						.queryParam(EndpointConstants.API_NASA_QUERY_PARAM, Constants.Q_PARAM))
				.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath(Constants.RESPONSE_JSON_PATH_VERSION)
						.value(Constants.SEARCH_NASA_RESPONSE_VERSION))
				.andExpect(MockMvcResultMatchers.jsonPath(Constants.RESPONSE_JSON_PATH_HREF)
						.value(Constants.SEARCH_NASA_RESPONSE_HREF));
	}

	/**
	 * 
	 * @return
	 */
	private NasaResponseDTO createSearchServiceResponse() {
		NasaResponseDTO nasaResponseDTO = new NasaResponseDTO();
		nasaResponseDTO.setCollection(new NasaCollectionDTO());
		nasaResponseDTO.getCollection().setVersion(Constants.SEARCH_NASA_RESPONSE_VERSION);
		nasaResponseDTO.getCollection().setHref(Constants.SEARCH_NASA_RESPONSE_HREF);
		return nasaResponseDTO;
	}

}

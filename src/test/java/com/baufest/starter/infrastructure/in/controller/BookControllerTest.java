package com.baufest.starter.infrastructure.in.controller;

import com.baufest.starter.application.command.service.BookCommandService;
import com.baufest.starter.application.query.service.BookQueryService;
import com.baufest.starter.domain.DTO.Book.BookDTO;

import com.baufest.starter.infrastructure.command.controller.BookController;
import com.baufest.starter.infrastructure.query.controller.BookQueryController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class BookControllerTest {

    @Mock
    private BookCommandService service;

    @Mock
    private BookQueryService queryService;

    @InjectMocks
    private BookController controller;

    @InjectMocks
    private BookQueryController queryController;

    @Test
    void books() {
        final String name = "book name";
        List<BookDTO> mock = Collections.singletonList(new BookDTO());
        Mockito.when(this.queryService.findByName(name)).thenReturn(mock);
        ResponseEntity<List<BookDTO>> entity = this.queryController.getBooks(name);
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertEquals(mock, entity.getBody());
    }

    @Test
    void bookError() {
        BookDTO mock = new BookDTO();
        int pages = -1;
        mock.setPages(pages);
        String error = "No puede ingresar un libro con " + pages + " paginas";
        Mockito.when(this.service.save(mock)).thenThrow(new IllegalArgumentException(error));
        ResponseEntity<?> entity = this.controller.saveBook(mock);
        assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
    }
}
package com.baufest.starter.domain.constants;

public class Constants {

	public static final String Q_PARAM = "moon";
	public static final String SEARCH_NASA_RESPONSE_VERSION = "1.0";
	public static final String SEARCH_NASA_RESPONSE_HREF = "http://images-api.nasa.gov/search?q=moon";
	
	public static final String RESPONSE_JSON_PATH_VERSION = "$.collection.version";
	public static final String RESPONSE_JSON_PATH_HREF = "$.collection.href";
	
}

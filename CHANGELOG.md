# Arquitectura Java de referencia
	Todos los cambios notables en este proyecto se documentarán en este archivo.
	El formato se basa en [Keep a Changelog] (https://keepachangelog.com).

## [Unreleased]

## [1.0.0] - 04-04-2022
Solución JAVA construida utilizando la implementación OpenJDK y Spring.

### Changed 
Migración a Java 17

### Added 
Se agregó caché a la validación del token JWT usando el punto final JWK

### Added 
Se agregó Spring webflux

### Added
Se agregó servicio Caché

### Added
Se agregó API Nasa Controller test

### Added 
Se agregó Search Service test

### Added 
Se agregó JavaDoc Standar


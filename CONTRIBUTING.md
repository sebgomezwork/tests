# Contribuir a la arquitectura de referencia

Gracias por tomarte el tiempo de contribuir! :+1: :tada:

### Tabla de contenidos

* [¿Cómo contribuir?](#cmo-contribuir)
    * [¿Dónde preguntar?](#dnde-preguntar)
    * [Crear un problema](#crear-un-problema)
    * [Submit a Pull Request](#submit-a-pull-request)
* [Participar en revisiones](#participar-en-revisiones)
* [¿Cómo ejecutar el código?](#cmo-ejecutar-el-cdigo)

### ¿Cómo contribuir?

#### ¿Dónde preguntar?

Si tienes alguna pregunta relacionada sobre las tecnologías usadas (Spring, Spring boot, etc.), puedes consultar
Stack Overflow usando
[esta lista de tags](https://stackoverflow.com/questions/tagged/spring+or+spring-mvc+or+spring-aop+or+spring-jdbc+or+spring-transactions+or+spring-annotations+or+spring-jms+or+spring-el+or+spring-test+or+spring+or+spring-remoting+or+spring-orm+or+spring-jmx+or+spring-cache+or+spring-webflux?tab=Newest) 
para encontrar una discusión existente o empezar una nueva si es necesario.

Si tú crees que hay un problema sobre la arquitectura de referencia de Baufest puedes
buscar a través de los 
[problemas existentes](https://gitlab.baufest.com/baufest-arquitecturas-de-referencia/java/-/issues) 
para intentar encontrar discusiones (pasadas o actuales), que están relacionadas 
con el problema. Leer estas discusiones ayudará a aprender sobre el problema y 
podremos tomar una mejor decisión.

#### Crear un problema

Reportar un problema o hacer una petición de característica es una gran manera de 
contribuir. Tu retroalimentación y las conversaciones que resultan de esta proveen un flujo
continuo de ideas. Sin embargo, antes de crear un ticket, por favor tomate el tiempo para
[preguntar e investigar](#dnde-preguntar) primero.

Si creas un problema después de una discusión en Stack Overflow, por favor provee una 
descripción en el problema en lugar de simplemente referir un enlace a Stack Overflow.
El rastreo del problema es un lugar importante de registrar las discusiones y debería ser
suficiente.

Una vez que estes listo, crea un problema en [GitLab](https://gitlab.baufest.com/baufest-arquitecturas-de-referencia/java/-/issues).

Muchos problemas son causados por comportamientos sutiles, errores tipográficos y 
configuraciones no deseadas. Crear un [ejemplo mínimo reproducible](https://stackoverflow.com/help/minimal-reproducible-example) 
del problema ayuda rápidamente al equipo a evaluar su problema y se llegue al núcleo 
del problema.

#### Submit a Pull Request 
[comment]: <> (todo)
1. Si tu no has sido agregado como contribuidor antes, puedes pedir que se te agregué a la
lista de contribuidores si tu merge request es aceptado.
2. ¿Deberías crear un problema primero? No, solo cree el merge request y use la 
descripción para proporcionar contexto y motivación, como lo haría con un problema. 
Si desea iniciar una discusión primero o ya ha creado un problema, una vez que se crea un 
merge request, cerraremos el problema como reemplazado del merge request, y la discusión 
sobre el problema continuará sobre el merge request.
3. Siempre revisa la rama `main` y haz el merge request sobre esta (para revisar las 
versiones ver el archivo [pom.xml](pom.xml))
4. Escoge conscientemente la granularidad de tus commits y squash commits que representan
multiples ediciones o correcciones del mismo cambió lógico. Revisa 
[Rewriting History section of Pro Git](https://git-scm.com/book/en/Git-Tools-Rewriting-History)
para obtener una descripción general de la simplificación del historial de commits.
5. El formato de los mensajes de commits debe ser siguiendo las convenciones de 
[Semantic Commit Messages](https://gist.github.com/joshbuchea/6f47e86d2510bce28f8e7f42ae84c716).
Puedes ver ejemplos usando ´git log´.
6. Si hay un problema anterior, haga referencia al número del problema de GitLab en la 
descripción del merge request.

Si se acepta, su contribución puede modificarse en gran medida según sea necesario antes de mezclarse.
Es probable que conserve la atribución de autor por sus commits de Git dado que la mayor parte de
sus cambios permanecen intactos. También se le puede pedir que vuelva a trabajar en el merge request.

Si se le solicita que realice correcciones, simplemente envíe los cambios a la misma rama y su 
merge request de cambios se actualizará. En otras palabras, no necesita crear un nuevo merge 
request cuando se le solicite realizar cambios.

#### Participar en revisiones

Ayudar a revisar los pull request es otra excelente manera de contribuir. Tu 
retroalimentación puede ayudar a dar forma a la implementación de nuevas características. 
Al revisar los pull request, sin embargo, absténgase de aprobar o rechazar un PR a menos 
que tenga los permisos.

### ¿Cómo ejecutar el código?

Desde el [README](README.md) puedes revisar como compilar y ejecutar el proyecto